import tkinter as tk
from tkinter import filedialog
import json
from zy_str_data import *
import os
import platform
from zy_code import *

def print_josn():
    json_data = json.dumps(ZY,ensure_ascii=False)
    root = tk.Tk()
    root.withdraw()
    print('\n ▊ 请在打开的窗口中，手动选择文件保存位置！')
    Folderpath = filedialog.askdirectory()
    title = '\周易.json'
    print('\n ▊ 选择的文件位置为：',Folderpath+title)
    print('\n ▊ 正在写入文件！')
    #json 文件格式写入
    w=open(Folderpath+title, 'w',encoding='utf8')
    w.write(json_data)
    #当文件读写格式为 ab 即追加时写入为该句 w.write(data.encode())
    w.close()
    print('\n ▊ 文件写入完成！请到：'+Folderpath+title+' 查看！')

def zy_input():
    put =  input('\n ▎ 周 易  ▎ >>> ')
    return put
    
def zy_ml():
    print(' ▊',end = ' ')
    for i in range(1,65):
        if i==64:
            print( '第'+str(i)+'卦 ' +'【'+ZY['64卦'][i][0]+'卦】',end = ' ')
        elif i%8 == 0:
            print( '第'+str(i)+'卦 ' +'【'+ZY['64卦'][i][0]+'卦】\n ▊',end = ' ')
        else:
            print( '第'+str(i)+'卦 ' +'【'+ZY['64卦'][i][0]+'卦】',end = ' ')


def pmdy():
    print('\n 《周 易》 \n\n======================================================================================\n')
    for i in range(1,65):
        bt = '【'+ZY['64卦'][i][0]+'卦】 '+ZY['64卦'][i][1]+' '+ZY['64卦'][i][2]
        
        jw = ZY['卦经'][i][0]
        
        szly = ZY['卦经'][i][1]
        
        ZY_N_G_X = list(ZY['数字八卦'].keys())[list(ZY['数字八卦'].values()).index(ZY['卦经'][i][1][0:3])]
        ZY_N_G_S = list(ZY['数字八卦'].keys())[list(ZY['数字八卦'].values()).index(ZY['卦经'][i][1][3:6])]

        
        sy = f'''▎ 释 义 ▎ ：
    
        『 {ZY['译文&释义'][i][1]} 』
            
        '''       
            
        fy = f'''▎ 译 文 ▎ ：
        
        {ZY['译文&释义'][i][0]}
        
        '''
        
        qt = f'''▎其 他  ▎：
            
        ▶ 数 字 六 爻：『 {szly} 』
        
        ▶ 卦 序 ：『 第 { i } 卦 』
        
        ▶ 上 卦 ： 『 {ZY_N_G_S} 』 {ZY['八卦'][ZY_N_G_S][0]}    数字三爻 『 {ZY['八卦'][ZY_N_G_S][1]} 』
                    
                    五行属 『 {ZY['八卦'][ZY_N_G_S][5]} 』  对应自然现象 『 {ZY['八卦'][ZY_N_G_S][4]} 』
        
                    ▊ 先天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_S][2][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_S][2][1]} 』方  
            
                    ▊ 后天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_S][3][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_S][3][1]} 』方
        
        ▶ 下 卦 ： 『 {ZY_N_G_X} 』 {ZY['八卦'][ZY_N_G_X][0]}    数字三爻 『 {ZY['八卦'][ZY_N_G_X][1]} 』
        
                    五行属 『 {ZY['八卦'][ZY_N_G_X][5]} 』  对应自然现象 『 {ZY['八卦'][ZY_N_G_X][4]} 』
        
                    ▊ 先天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_X][2][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_X][2][1]} 』方  
            
                    ▊ 后天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_X][3][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_X][3][1]} 』方
        '''
        
        print(bt)
        print(jw,'\n')
        print(sy)
        print(fy)
        print(qt)
        print('\n======================================================================================\n')


def print_txt():
    troot = tk.Tk()
    troot.withdraw()
    print('\n ▊ 请在打开的窗口中，手动选择文件保存位置！')
    tFolderpath = filedialog.askdirectory()
    ttitle = '\周易.txt'
    print('\n ▊ 选择的文件位置为：',tFolderpath+ttitle)

    fp = open(tFolderpath+ttitle, 'w', encoding = 'utf-8')
    print('\n ▊ 正在写入文件！\n')
    print('\n 《周 易》 \n\n======================================================================================\n',file = fp)
    print(zyjj[0:zyjj.find('▶')],file = fp)
    print('======================================================================================\n',file = fp)
    print('『目 录 』：\n',file = fp)
    print(' ▊',end = ' ',file = fp)
    for i in range(1,65):
        if i==64:
            print( '第'+str(i)+'卦 ' +'【'+ZY['64卦'][i][0]+'卦】',end = ' ',file = fp)
        elif i%8 == 0:
            print( '第'+str(i)+'卦 ' +'【'+ZY['64卦'][i][0]+'卦】\n ▊',end = ' ',file = fp)
        else:
            print( '第'+str(i)+'卦 ' +'【'+ZY['64卦'][i][0]+'卦】',end = ' ',file = fp)

    print('\n\n======================================================================================\n',file = fp)

    for i in range(1,65):
        bt = '【'+ZY['64卦'][i][0]+'卦】 '+ZY['64卦'][i][1]+' '+ZY['64卦'][i][2]

        jw = ZY['卦经'][i][0]

        szly = ZY['卦经'][i][1]

        ZY_N_G_X = list(ZY['数字八卦'].keys())[list(ZY['数字八卦'].values()).index(ZY['卦经'][i][1][0:3])]
        ZY_N_G_S = list(ZY['数字八卦'].keys())[list(ZY['数字八卦'].values()).index(ZY['卦经'][i][1][3:6])]

        sy = f'''▎ 释 义 ▎ ：
    
『 {ZY['译文&释义'][i][1]} 』
            
        '''       
            
        fy = f'''▎ 译 文 ▎ ：
        
        {ZY['译文&释义'][i][0]}
        
        '''

        qt = f'''▎其 他  ▎：

        ▶ 数 字 六 爻：『 {szly} 』

        ▶ 卦 序 ：『 第 { i } 卦 』

        ▶ 上 卦 ： 『 {ZY_N_G_S} 』 {ZY['八卦'][ZY_N_G_S][0]}    数字三爻 『 {ZY['八卦'][ZY_N_G_S][1]} 』

                    五行属 『 {ZY['八卦'][ZY_N_G_S][5]} 』  对应自然现象 『 {ZY['八卦'][ZY_N_G_S][4]} 』

                    ▊ 先天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_S][2][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_S][2][1]} 』方  

                    ▊ 后天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_S][3][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_S][3][1]} 』方

        ▶ 下 卦 ： 『 {ZY_N_G_X} 』 {ZY['八卦'][ZY_N_G_X][0]}    数字三爻 『 {ZY['八卦'][ZY_N_G_X][1]} 』

                    五行属 『 {ZY['八卦'][ZY_N_G_X][5]} 』  对应自然现象 『 {ZY['八卦'][ZY_N_G_X][4]} 』

                    ▊ 先天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_X][2][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_X][2][1]} 』方  

                    ▊ 后天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_X][3][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_X][3][1]} 』方
        '''

        print(bt,file = fp)
        print(jw,'\n',file = fp)
        print(sy,file = fp)
        print(fy,file = fp)
        print(qt,file = fp)
        print('\n======================================================================================\n',file = fp)

    fp.close()
    print(' ▊ 文件写入完成！请到：'+tFolderpath+ttitle+' 查看！\n')


def print_md():
    troot = tk.Tk()
    troot.withdraw()
    print('\n ▊ 请在打开的窗口中，手动选择文件保存位置！')
    tFolderpath = filedialog.askdirectory()
    ttitle = '\周易.md'
    print('\n ▊ 选择的文件位置为：',tFolderpath+ttitle)

    fp = open(tFolderpath+ttitle, 'w', encoding = 'utf-8')
    print('\n ▊ 正在写入文件！\n')
    
    print('\n # **周 易** \n\n--- \n',file = fp)
    print(zyjj[0:zyjj.find('▶')],file = fp)
    print('--- \n',file = fp)
    print('『目 录 』：\n',file = fp)
    print('[TOC]',file = fp)
    print('---  \n',file = fp)
    
    for i in range(1,65):
        bt = '## 【'+ZY['64卦'][i][0]+'卦】 '+ZY['64卦'][i][1]+' '+ZY['64卦'][i][2]

        jw = ZY['卦经'][i][0]

        szly = ZY['卦经'][i][1]

        ZY_N_G_X = list(ZY['数字八卦'].keys())[list(ZY['数字八卦'].values()).index(ZY['卦经'][i][1][0:3])]
        ZY_N_G_S = list(ZY['数字八卦'].keys())[list(ZY['数字八卦'].values()).index(ZY['卦经'][i][1][3:6])]

        sy = f'''▎ 释 义 ▎ ：
    
        『 {ZY['译文&释义'][i][1]} 』
            
        '''       
            
        fy = f'''▎ 译 文 ▎ ：
        
```
        {ZY['译文&释义'][i][0]}
        
```
        
        '''

        qt = f'''▎其 他  ▎：

        ▶ 数 字 六 爻：『 {szly} 』

        ▶ 卦 序 ：『 第 { i } 卦 』

        ▶ 上 卦 ： 『 {ZY_N_G_S} 』 {ZY['八卦'][ZY_N_G_S][0]}    数字三爻 『 {ZY['八卦'][ZY_N_G_S][1]} 』

                    五行属 『 {ZY['八卦'][ZY_N_G_S][5]} 』  对应自然现象 『 {ZY['八卦'][ZY_N_G_S][4]} 』

                    ▊ 先天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_S][2][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_S][2][1]} 』方  

                    ▊ 后天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_S][3][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_S][3][1]} 』方

        ▶ 下 卦 ： 『 {ZY_N_G_X} 』 {ZY['八卦'][ZY_N_G_X][0]}    数字三爻 『 {ZY['八卦'][ZY_N_G_X][1]} 』

                    五行属 『 {ZY['八卦'][ZY_N_G_X][5]} 』  对应自然现象 『 {ZY['八卦'][ZY_N_G_X][4]} 』

                    ▊ 先天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_X][2][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_X][2][1]} 』方  

                    ▊ 后天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_X][3][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_X][3][1]} 』方
        '''

        print(bt,file = fp)
        print(jw,'\n',file = fp)
        print(sy,file = fp)
        print(fy,file = fp)
        print(qt,file = fp)
        print('\n--- \n',file = fp)

    fp.close()
    print(' ▊ 文件写入完成！请到：'+tFolderpath+ttitle+' 查看！\n')

def zy_cx(i):
    print('\n 《周 易》 \n\n======================================================================================\n')

    bt = '【'+ZY['64卦'][i][0]+'卦】 '+ZY['64卦'][i][1]+' '+ZY['64卦'][i][2]

    jw = ZY['卦经'][i][0]

    szly = ZY['卦经'][i][1]

    ZY_N_G_X = list(ZY['数字八卦'].keys())[list(ZY['数字八卦'].values()).index(ZY['卦经'][i][1][0:3])]
    ZY_N_G_S = list(ZY['数字八卦'].keys())[list(ZY['数字八卦'].values()).index(ZY['卦经'][i][1][3:6])]

    sy = f'''▎ 释 义 ▎ ：
    
    『 {ZY['译文&释义'][i][1]} 』
        
    '''       
        
    fy = f'''▎ 译 文 ▎ ：
    
    {ZY['译文&释义'][i][0]}
    
    '''

    qt = f'''▎其 他  ▎：

    ▶ 数 字 六 爻：『 {szly} 』

    ▶ 卦 序 ：『 第 { i } 卦 』

    ▶ 上 卦 ： 『 {ZY_N_G_S} 』 {ZY['八卦'][ZY_N_G_S][0]}    数字三爻 『 {ZY['八卦'][ZY_N_G_S][1]} 』

                五行属 『 {ZY['八卦'][ZY_N_G_S][5]} 』  对应自然现象 『 {ZY['八卦'][ZY_N_G_S][4]} 』

                ▊ 先天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_S][2][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_S][2][1]} 』方  

                ▊ 后天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_S][3][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_S][3][1]} 』方

    ▶ 下 卦 ： 『 {ZY_N_G_X} 』 {ZY['八卦'][ZY_N_G_X][0]}    数字三爻 『 {ZY['八卦'][ZY_N_G_X][1]} 』

                五行属 『 {ZY['八卦'][ZY_N_G_X][5]} 』  对应自然现象 『 {ZY['八卦'][ZY_N_G_X][4]} 』

                ▊ 先天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_X][2][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_X][2][1]} 』方  

                ▊ 后天八卦 ▊ 居 『 {ZY['八卦'][ZY_N_G_X][3][0]} 』宫 ，位 『 {ZY['八卦'][ZY_N_G_X][3][1]} 』方
    '''

    print(bt)
    print(jw,'\n')
    print(sy)
    print(fy)
    print(qt)
    print('\n======================================================================================\n')

def tgdz():
    ganzhi = ['']*60
    print("\n    ▊ 输入『td』输出天干地支表；输入『exit』退出查询，输入年份查询对应的天干地支纪年！\n")

    while True:
        
        year = input('    PS ▎天干地支  ▎ >>> ')
        
        if year == 'exit' :
            print("\n     ▊ 天干地支纪年转换与查询结束！\n")
            break
        elif year == 'td':
            for i in range(60):
                ganzhi[i] = T[i%10]+D[(i)%12]
            print("\n     ▊ ▊ 天干地支表为：\n \n {} \n \n   总数为:{} ,即为60甲子。\n\n".format(ganzhi,len(ganzhi)))
            continue
        
        elif year.isdigit() and len(year)==4:
            
            N = (int(year)-3)%60
            t = N%10
            d = N%12
        
        else:
            print('\n     ▊ 查询命令不正确，请重新输入！ ▊ \n')
            continue
                
        print('\n     ▊ 公元 『{0}』 年是属天干地支的 『{1}{2}』 年\n'.format(year,T[t-1],D[d-1]))


def  clear():
    # 返回系统平台/OS的名称，如Linux，Windows，Java，Darwin
    system = platform.system()
    if (system == u'Windows'):
        os.system('cls')
    else:
        os.system('clear')

def main():
    print(zystr)
    print(zyjj)

    while True:
        put = zy_input()
        if (put == '' or put == ' ' or put not in zy_code  ):
            continue
            
        if (put == 'exit' or put == 'exit()' ):
            print('\n')
            break
            
        if (put == 'ZY -h' or put == 'zy -h'):
            print(zy_help)
            
        if (put == 'zy -m'):
            print('\n')
            zy_ml()
            print('\n')
            
        if (put == 'zy -c'):
            st_cx = input('\n  PS『查 询』请输入卦名或挂号 >>> ')
            print('\n')
            #isdigit()判断输入是否为数字
            if st_cx.isdigit():
                i = int(st_cx)
                zy_cx(i)

            else:
                for n in range(1,65):
                    if st_cx in ZY['64卦'][n][0]:
                        i = int(n)
                        zy_cx(i)

        if (put == 'zy -j'):
            print_josn()
            print('\n')
            
        if (put == 'zy -t'):
            pmdy()
            print('\n 内容输出完毕 \n')
            
        if (put == 'zy -txt'):
            print_txt()

        if (put == 'zy -md'):
            print_md()
            
        if (put == 'zy -td'):
            tgdz()

        if (put == 'cl' or put == 'clear' ):
            clear()
